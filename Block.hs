{- |
 - Module      :  Block
 - Description :  An array-like data structure that remembers past states.
 - Copyright   :  (c) 2018-2019 William Tracy
 -
 - Maintainer  :  afishionado@gmail.com
 - Stability   :  experimental 
 - Portability :  portable 
 -
 - The Block module revolves around the Block datatype. Blocks can be created
 - from an arbitrary list of type Pixel8. (Pixel8 is defined in the JuicyPixels
 - package.) Values inside a Block can be read and arbitrarily reordered. Each
 - reordering operation causes a copy of the previous state to be stored
 - internally. Blocks can also be rendered as two-dimensional images. This can
 - take the form of a flat Vector with pixel data with rows stored sequentially,
 - or in the form of a ready-to-use JuicyPixels Image.
 - -}
 
module Block (
  Block, start, get, swap, move, size, forEach, forEachStep, export, exportImage, isDone, 
  steps, compareSwap, compareSwapNeighbors
) where

import Data.Vector.Storable as V
import Data.Foldable as F
import Data.Sequence as S
import Codec.Picture
import Control.Exception.Base

-- | The Block type represents a sequence of values, as well as all previous 
-- states of that sequence.
-- | Internally, the most recent state is at the head, and the original state
-- is at the tail.
type Block = [Seq Pixel8]

-- | Creates a Block from a starting list.
start::[Pixel8]->Block
start input = [S.fromList input]

-- | Retrieve the current value at location i.
get::Block->Int->Pixel8
get block i = 
  assert (i >= 0 && i < (size block))
    (Prelude.head block) `index` i

-- | The length of the data available for reading and writing.
size::Block->Int
size block = S.length (Prelude.head block)
    
-- | True if all the data in the block is sorted in ascending order.
-- False otherwise.
isDone::Block->Bool
isDone block = isDoneStep (Prelude.head block) 0

-- | Helper for forEach. Executes func once for each value from i through the
-- last index in the block if step is equal to 1. Otherwise, advances step 
-- number of indices each time. Except for not starting at zero, behaves the 
-- same as forEachStep.
doForEach::Block->(Block->Int->Block)->Int->Int->Block
doForEach block func i step
  | i >= (size block) = block
  | otherwise = doForEach (func block i) func (i + step) step

-- | Executes the passed function once for each value in the block.
-- The function is passed the current state of the block, and the "current"
-- block index. It is expected to return and updated version of the block,
-- which is passed to the next invocation. Returns the result of the final
-- invocation.
forEach::Block->(Block->Int->Block)->Block
forEach block func = doForEach block func 0 1

-- | Like forEach, but only executes on every step-th value in the block.
forEachStep::Block->(Block->Int->Block)->Int->Block
forEachStep block func step = doForEach block func 0 step

-- | Returns a new block equivalent to the passed block with the values at
-- the ith and jth indeces switched.
swap::Block->Int->Int->Block
swap block i j =
  let 
    latest = Prelude.head block
    intermediate = (S.update j (index latest i) latest) 
  in
    (S.update i (latest `index` j)
      intermediate) : block

-- | Drops the ith index from the current state of the block. Does not push
-- state to the block history. Tread with care.
remove::Seq a->Int->Seq a
remove sequence i =
    (S.take i sequence) >< (S.drop (i + 1) sequence)

-- | Inserts a new value at the ith location in the block. Does not push
-- state to the block history. Tread with care.
insert::Seq a->Int->a->Seq a
insert sequence index value =
  let
    front = S.take index sequence
    rear = S.drop index sequence
  in ((front |> value) >< rear)

-- | Returns the passed block with the ith value moved to the jth location.
-- All values between index i and index j will be effectively moved one 
-- address over.
move block i j =
  let
    previous = Prelude.head block
    value = (index previous i)
  in
    (insert (remove previous i) j value) : block

-- | Serializes the current and all past states of the block to a Vector.
export::Block->Vector Pixel8
export block = V.fromList (F.toList flattened)
  where 
    flattened = (Prelude.foldl (flip (><)) S.empty block)

-- | Generates an image based on the given Block. The original Block state 
-- will be written to the first row of the image, the most recent state will
-- be written to the bottom row of the image.
exportImage::Block->DynamicImage
exportImage block = 
  let 
    result = Image {
      imageWidth = (size block),
      imageHeight = steps block,
      imageData = export block
    }
  in
    assert ((V.length (imageData result)) == 
      (size block) * (steps block)) (ImageY8 result)

-- | True if all the data in the list is sorted in ascending order.
-- False otherwise.
isSorted::[Pixel8]->Bool
isSorted [] = True
isSorted (_:[]) = True
isSorted (a:b:remainder) =
  if a > b
    then False
    else isSorted (b:remainder)

-- | Helper for isDone. True IFF everything from i through the end of the
-- Seq is monotonically increasing.
isDoneStep::Seq Pixel8->Int->Bool
isDoneStep pixels i = 
  if (i < S.length pixels - 1) then
    ((index pixels  i) <= (index pixels  (i + 1))) && isDoneStep pixels (i + 1)
  else
    True

-- | Returns the number of states in the Block's history.
steps::Block->Int
steps block = Prelude.length block

-- | Compares the values and indeces i and j. If the value at i is greater than
--  the value at j, the values at i and j are switched.
compareSwap::Block->Int->Int->Block
compareSwap block i j =
    if (get block i) > (get block j) then
      swap block i j
    else
      block

-- | Compares the values at indeces i and i+1. If the value at i+1 is greater 
-- than the value i, the values at i and i+1 are switched.
compareSwapNeighbors::Block->Int->Block
compareSwapNeighbors block i = compareSwap block i (i + 1)

