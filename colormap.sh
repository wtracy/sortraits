#!/bin/bash

# Applies the specified palette to all PNG images in the current directory, and 
# saves the result under a directory named colormap. Expects all images in the 
# working directory to be grayscale.


cd output
mkdir -p colormap/difference
mkdir -p colormap/polar/difference
palette="../palettes/redpink.png"


parallel --bar convert {} $palette -clut colormap/{} ::: \
  *.png polar/*.png difference/*.png polar/difference/*.png

