#!/bin/bash

# Generates polar-coordinate transformations of raw and difference 
# visualizations.

# Output image width/height in pixels. Should be an even integer.
cd output
let size=6089
mkdir -p polar/difference

# We need something interesting to fill the space around the circle-shaped 
# polar visualization. We'll fill it with a set of sorted values extending to 
# the image edges. The line below generates such a gradient, converts it to 
# polar coordinates at 2048x2048 resolution, and crops it down to 1024x1024, 
# eliminating the black dead zones. The result appears to extend to infinity.
echo "Generating template..."
let "double=$size * 2"
let "half=$size / 2"
convert -size "$double"x"$double" -define gradient:direction=West gradient: \
  -virtual-pixel HorizontalTile -background Black -distort Polar 0 +repage \
  -crop "$size"x"$size"+"$half"+"$half" temp.png

# Perform the polar transform on available images, and composite them over the 
# "template" we just created.
echo "Converting raw visualizations..."
parallel convert {} -alpha on -scale "$size"x"$size"\! +repage \
  -virtual-pixel HorizontalTile -background Transparent +distort Polar 0 \
  +repage  polar/{} ::: *.png

echo "Compositing backgrounds..."
parallel composite -compose over polar/{} temp.png polar/{} ::: *.png

# Convert the difference visualizations. Since sorted segments show up black, 
# extending outward with black isn't noticeably "wrong" and we can skip the 
# compositing step.
echo "Converting difference visualizations..."
parallel convert {} -scale "$size"x"$size"\! +repage \
  -virtual-pixel HorizontalTile -background Black +distort Polar 0 \
  +repage polar/{} ::: difference/*.png

rm temp.png
